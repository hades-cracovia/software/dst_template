#!/bin/bash

# change hydra version if needed
ver="4.9v"

. /cvmfs/hades.gsi.de/install/5.34.34/hydra2-${ver}/defall.sh
export HGEANT_DIR=/cvmfs/hades.gsi.de/install/5.34.34/hgeant2-${ver}/

