#!/usr/bin/env python3

# modified by R. Lalik

import os,sys,glob
import argparse
import shlex, subprocess

def_time = 90
def_mem = "5000mb"
def_events = 100000000
def_script='job_script.sh'

parser=argparse.ArgumentParser(description='Submit jobs to GSI batch farm')
parser.add_argument('arguments', help='list of arguments', type=str, nargs='+')
parser.add_argument('-a', '--array', help='send as array job', nargs='?', default=False)
parser.add_argument('-p', '--part', help='partition', type=str, default="main")
parser.add_argument('-f', '--file', help='input is single file', action='store_true', default=False)
parser.add_argument('-e', '--events', help='number of events per file to be processed',type=int, default=def_events)
parser.add_argument('-t', '--time', help='time need to finish task', type=int, default=def_time)
parser.add_argument('-m', '--mem', help='requested memory', type=str, default=def_mem)
parser.add_argument('-s', '--script', help='execute script', type=str, default=def_script)
parser.add_argument('-d', '--directory', help='output directory', type=str, default='.')
parser.add_argument('-n', '--files',help='number of files you want to proceed', type=int, default=-1)
parser.add_argument('-l', '--split',help='split jobs into max packages', type=int, default=0)
parser.add_argument('-v', '--verbose', help='verbose mode', action='store_true', default=False)
parser.add_argument('--pretend', help='pretend mode, do not send actuall jobs', action='store_true', default=False)

args=parser.parse_args()

if args.verbose:
    print(args)

submissiondir = os.getcwd()+'/'
tpl_resources = '--time={0:1d}:{1:02d}:00 --mem-per-cpu={2:s} -p main'
jobscript = submissiondir + args.script

if __name__=="__main__":
    i=0

    resources = tpl_resources.format(int(args.time/60), args.time % 60, args.mem)
    lines = []

    events = args.events

    deps = [None] * args.split
    for arg in args.arguments:
        if args.file is False:
            f = open(arg)
            lines = f.readlines()
            f.close()
        else:
            lines = [ arg ]

        jobid_list = ""

        if not args.array == False:
            if args.file:
                print("--file nanot be used with arrays")
            else:
                array_arg = ""
                if args.array == None:
                    with open(arg) as ff:
                        num_lines = sum(1 for _ in ff)
                    print(num_lines)
                    array_args = "--array=0-{:d}".format(num_lines-1)
                elif args.array[0] == '%':
                    with open(arg) as ff:
                        num_lines = sum(1 for _ in ff)
                    array_args = "--array=0-{:d}{:s}".format(num_lines-1, args.array)
                else:
                    array_args = "--array={:s}".format(args.array)

                logfile = submissiondir + '/log/slurm-%A_%a-array.log'

                if os.path.isfile(logfile):
                    os.remove(logfile)

                command = "{:s} -o {:s} {:s} --export=\"pattern={:s},events={:d},odir={:s}\"".format(array_args, logfile, resources, arg, events, args.directory)
                command += " " + jobscript

                job_command='sbatch ' + command + ' -vvv'
                print(job_command)

                if not args.pretend:
                    proc = subprocess.Popen(shlex.split(job_command), stdout=subprocess.PIPE, shell=False)
                    (out, err) = proc.communicate()
                    if (out[0:19] == b'Submitted batch job'):
                        jobid = out[20:-1].decode()
                    else:
                        print("Job failed with error:")
                        print(err)
                else:
#                    print(job_command)
                    pass

        else:
            for entry in lines:
                entry = entry.strip()

                i += 1

                logfile = submissiondir + '/log/slurm-%j.log'

                if os.path.isfile(logfile):
                    os.remove(logfile)

                command = "-o {:s} {:s} -J {:s} --export=\"pattern={:s},events={:d},odir={:s}\"".format(logfile, resources, os.path.split(entry)[1], entry, events, args.directory)
                if (args.split > 0 and deps[i % args.split] != None):
                    command += " -d afterany:"+deps[i % args.split]
                command += " " + jobscript

                job_command='sbatch ' + command
                print(job_command)

                if not args.pretend:
                    proc = subprocess.Popen(shlex.split(job_command), stdout=subprocess.PIPE, shell=False)
                    (out, err) = proc.communicate()
                    if (out[0:19] == b'Submitted batch job'):
                        jobid = out[20:-1].decode()
                        if (args.split > 0):
                            deps[i % args.split] = jobid
                    else:
                        print("Job failed with error:")
                        print(err)
                else:
#                    print(job_command)
                    pass

            print(i, ' entries submitted')
