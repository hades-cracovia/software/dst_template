#!/bin/bash

exp="dst_template"

date

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY: ${SLURM_ARRAY_TASK_ID} in ${pattern}"
    file=$(sed -n $((${SLURM_ARRAY_TASK_ID}+1))p ${pattern})
else
    echo "NO ARRAY"
    file=$pattern
fi

. /lustre/nyx/hades/user/${USER}/hades/${exp}/profile.sh

odir=$(readlink -e $odir)
file=$(readlink -e $file)

echo file=$file
echo events=$events
echo odir=$odir

root -b -q

cd /lustre/nyx/hades/user/${USER}/hades/${exp}/sim/geant

date

./analysisDST $file $odir 1

echo ./analysisDST $file ./ $events
time ./analysisDST $file ./ $events

mv $(basename $file .root)*.root $odir -v
