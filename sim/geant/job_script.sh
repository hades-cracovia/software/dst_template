#!/bin/bash

exp="dst_template"

date

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY: ${SLURM_ARRAY_TASK_ID} in ${pattern}"
    file=$(sed -n $((${SLURM_ARRAY_TASK_ID}+1))p ${pattern})
else
    echo "NO ARRAY"
    file=$pattern
fi

. /lustre/nyx/hades/user/${USER}/hades/${exp}/profile.sh

odir=$(readlink -e $odir)
#file=$(readlink -e $file)

echo file=$file
echo events=$events
echo odir=$odir
root -b -q

cd /lustre/nyx/hades/user/${USER}/hades/${exp}/sim/geant

card_file=/tmp/geaini_p4500p_fwdet__$(basename ${file} .evt).ini

date

truncate -s0 ${odir}/$(basename ${file} .evt)_1.root

# update placeholders and generate temporary file
sed \
    -e "s|PWD|$(pwd)|g" \
    -e "s|@input@|${file}|" \
    -e "s|@output@|$(basename ${file} .evt)_.root|" \
    simul_p4500p_fwdet.dat > ${card_file}

tmpdirname=$(mktemp -d)
cd ${tmpdirname}
pwd

if [ -z ${HGEANT_DIR} ]; then
    echo hgeant -b -c -f ${card_file}
    time hgeant -b -c -f ${card_file}
else
    echo ${HGEANT_DIR}/hgeant -b -c -f ${card_file}
    time ${HGEANT_DIR}/hgeant -b -c -f ${card_file}
fi

mv $(basename ${file} .evt)_*.root ${odir}/ -v

echo "*********************************************"
cat ${card_file}
echo "*********************************************"

cd -
rm -rfv ${tmpdirname}
rm -fv ${card_file}
