#ifndef ASCII_TABLE
#define ASCII_TABLE

#include <map>

//std::map<int, int> fParticleTableASCII;

fParticleTableASCII[22]		= 1;
fParticleTableASCII[-11]	= 2;
fParticleTableASCII[11]		= 3;
fParticleTableASCII[-13]	= 5;
fParticleTableASCII[13]		= 6;
fParticleTableASCII[111]	= 7;
fParticleTableASCII[211]	= 8;
fParticleTableASCII[-211]	= 9;
fParticleTableASCII[130]	= 10;
fParticleTableASCII[321]	= 11;
fParticleTableASCII[-321]	= 12;
fParticleTableASCII[2112]	= 13;
fParticleTableASCII[2212]	= 14;
fParticleTableASCII[-2212]	= 15;
fParticleTableASCII[310]	= 16;
fParticleTableASCII[221]	= 17;
fParticleTableASCII[3122]	= 18;
fParticleTableASCII[3222]	= 19;
fParticleTableASCII[3212]	= 20;
fParticleTableASCII[3112]	= 21;
fParticleTableASCII[3322]	= 22;
fParticleTableASCII[3312]	= 23;
fParticleTableASCII[3334]	= 24;
fParticleTableASCII[-2112]	= 25;
fParticleTableASCII[-3122]	= 26;
fParticleTableASCII[-3112]	= 27;
fParticleTableASCII[-3212]	= 28;
fParticleTableASCII[-3222]	= 29;
fParticleTableASCII[-3322]	= 30;
fParticleTableASCII[-3312]	= 31;
fParticleTableASCII[-3334]	= 32;
fParticleTableASCII[113]	= 41;
fParticleTableASCII[213]	= 42;
fParticleTableASCII[-213]	= 43;
//fParticleTableASCII[0]		= 45;
//fParticleTableASCII[0]		= 46;
//fParticleTableASCII[0]		= 47;
//fParticleTableASCII[0]		= 49;
fParticleTableASCII[3334]	= 52;
fParticleTableASCII[331]	= 53;
fParticleTableASCII[333]	= 55;
fParticleTableASCII[443]	= 67;

#endif
