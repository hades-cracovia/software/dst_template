#include "UrqmdEvent.h"

#include <cstdlib>
#include <ctime>

UrqmdEventReader::UrqmdEventReader() : fInputFile(NULL)
{
#include "ascii_pdg.h"
#include "urqmd_pdg.h"
	srand(time(0));
}

UrqmdEventReader::~UrqmdEventReader()
{
}
/*
void UrqmdEventReader::ReadConversionTable() {
	gRandom = new TRandom();

//	TString work      = getenv("VMCWORKDIR");
//	TString fileName  = work + "/input/urqmd_pdg.dat";

	TString fileName  = "urqmd_pdg.dat";
	TString fileNameA = "ascii_pdg.dat";

	ifstream* pdgconv = new ifstream(fileName.Data());
	ifstream* asciiconv = new ifstream(fileNameA.Data());

	int index = 0;
	int pdgId = 0;
	int asciiId = 0;

	std::map<int,int> fParticleTableASCII;      //!  Map from UrQMD PID to PDGPID

	while ( ! asciiconv->eof() ) {
		index = asciiId =0 ;
		*asciiconv >> asciiId >> pdgId;
		fParticleTableASCII[pdgId] = asciiId;
	}

	while ( ! pdgconv->eof() ) {
		index =pdgId =0 ;
		*pdgconv >> index >> pdgId;
		if (fParticleTableASCII.count(pdgId))
			fParticleTable[index] = fParticleTableASCII[pdgId];
	}

	std::cout << "-I CbmUrqmdGenerator: Particle table for conversion from "
		<< "UrQMD loaded" <<  std::endl;
}
*/

//******************************
void UrqmdEventReader::openFile(const std::string & file ) {
//	const char * file = "/scratch/e12m/knucl/HADES/urqmd/CBe_imp2.0/inputfile15_0.ftn14";

	if (fInputFile) {
		fclose(fInputFile);
		fInputFile = NULL;
	}

	printf("-I UrqmdEventReader: Opening input file |%s|\n", file.c_str());
	fInputFile = fopen(file.c_str(), "r");
	if ( !fInputFile ) {
		printf("Fatal: Cannot open input file.\n");
		exit(EXIT_FAILURE);
	}

//	ReadConversionTable();
//	PR(file.c_str());
	counter = 0;
}

void UrqmdEventReader::closeFile()
{
	fclose(fInputFile);
	fInputFile = NULL;
}

UrqmdEvent * UrqmdEventReader::getEvent() {
	if (!fInputFile)
		return NULL;
//		openFile();

	// ---> Define event variables to be read from file
	int evnr=0, ntracks=0, aProj=0, zProj=0, aTarg=0, zTarg=0;
	float beta = 0., b = 0., phi = 0., ekin = 0., plab = 0.;
	float betaNN = 0., beta_pro = 0.;
	int encol = 0;

	char urqmd_name[1024];
	int urqmd_version = 0;
	int ityp=0, i3=0, ichg=0, pid=0;
	float ppx=0., ppy=0., ppz=0., m=0.;
	float x=0., y=0., z=0.;

	int lcl = 0, ncol = 0;

	// ---> Read and check first event header line from input file
	char read[200];
	fscanf(fInputFile, "%s %*s %d %*s %*s %*s %*s", urqmd_name, &urqmd_version);
	if ( feof(fInputFile) )
	{
		fclose(fInputFile);
		fInputFile = NULL;
		return NULL;
	}
	if ( urqmd_name[0] != 'U' )
	{
		return NULL;
	}

	// ---> Read rest of event header
	// Line 2: projectile and target
	fscanf(fInputFile, "%*s %*s %*s %d %d %*s %*s %*s %d %d", &aProj, &zProj, &aTarg, &zTarg);
	fgets(read, 200, fInputFile);
	// Line 3: Lab transformation
	fscanf(fInputFile, "%*s %*s %*s %e %e %e", &betaNN, &beta, &beta_pro);
	fgets(read, 200, fInputFile);
	// Line 4: Impact parameter
	fscanf(fInputFile, "%*s %e %*e %*e %*s %*e", &b);
	fgets(read, 200, fInputFile);
	// Line 5: Lab energy and momentum
	fscanf(fInputFile, "%*s %*d %*s %e %*s %*e %*s %e", &ekin, &plab);
	fgets(read, 200, fInputFile);
	// Line 6: Event number
	fscanf(fInputFile, "%*s %d %*s %*s %*d %*s %*s %*d %*e %*d ", &evnr);
	fgets(read, 200, fInputFile);
	// Skip next 8 lines
	int iline_limit = 0;

	switch (urqmd_version)
	{
		case 20030:
			iline_limit = 8;
			break;
		default:
			iline_limit = 11;
			break;
	}

	for (int iline = 0; iline < iline_limit; ++iline)
		fgets(read, 200,fInputFile);
	// Line 15: Number of tracks
	fscanf(fInputFile, "%d", &ntracks);
	fgets(read, 200, fInputFile);
	// Line 16: Number of collisions
	fscanf(fInputFile, "%d", &encol);
	fgets(read, 200, fInputFile);

	// ---> Calculate gamma for Lorentztransformation
	double gamma = sqrt( 1. / ( 1. - beta*beta) );

	// ---> Generate rotation angle	D
//	if ( fEventPlaneSet ) phi = gRandom->Uniform(fPhiMin, fPhiMax);

//	printf("-I CbmUrqmdGenerator: Event %d , b = %e fm,	multiplicity %d , ekin: %e GeV, plab: %e GeV, beta: %e , phi: %e\n", evnr, b, ntracks, ekin, plab, beta, phi);

	// Set event id and impact parameter in MCEvent if not yet done
/*	CbmMCEventHeader* event = dynamic_cast<CbmMCEventHeader*>(primGen->GetEvent());
	if ( event && (! event->IsSet()) ) {
		event->SetEventID(evnr);
		event->SetB(b);
		event->SetPhi(phi);
		event->SetNPrim(ntracks);
		event->MarkSet(true);
	}
*/
	++counter;
	int digi = floor(log10(counter));
	if ((digi > 2) && (fmod(counter, pow(10, double(digi))) == 0))
		std::cout << "event: " << counter << std::endl;
//		PR(counter);

	// ---> Loop over tracks in the current event
	int itrack;
//	uevt->evt_ = 0; TODO

	UrqmdEvent * uevt = new UrqmdEvent(ntracks);
	uevt->evtnum = evnr;
	uevt->Ebeam = ekin;
	uevt->bpar = b;
	uevt->ncols = encol;
	for(itrack=0; itrack<ntracks; itrack++) {
		
		// Read momentum and PID from file
//		fgets(read, 17, fInputFile);
		fscanf(fInputFile, "%*e %e %e %e %*e %e %e %e %e %d %d %d %d %d",
				&x, &y, &z, &ppx, &ppy, &ppz, &m, &ityp, &i3, &ichg, &lcl, &ncol);
/*		fscanf(fInputFile, "%e", &y);
		fscanf(fInputFile, "%e", &z);
//PR(x); PR(y); PR(z);
//		fgets(read, 81, fInputFile);
		fscanf(fInputFile, "%e", &ppx);	// fake
//PR(ppx);

		fscanf(fInputFile, "%e", &ppx);
		fscanf(fInputFile, "%e", &ppy);
		fscanf(fInputFile, "%e", &ppz);
		fscanf(fInputFile, "%e", &m);
		fscanf(fInputFile, "%d", &ityp);
		fscanf(fInputFile, "%d", &i3);
		fscanf(fInputFile, "%d", &ichg);
		fscanf(fInputFile, "%d", &lcl);
		fscanf(fInputFile, "%d", &ncol);
*/
		fgets(read, 200, fInputFile);
//PR(ppx); PR(ppy); PR(ppz); PR(m);
//PR(ityp); PR(i3); PR(ichg);
//PR(lcl); PR(ncol);
//		printf("R: %e %e %e %e %e %e %e %d %d %d %d %d\n", x, y, z, ppx, ppy, ppz, m, ityp, i3, ichg, lcl, ncol);

//		if (ncol == 0)
//			continue;

		// Convert UrQMD type and charge to unique pid identifier
		if (ityp >= 0) pid =	1000 * (ichg+2) + ityp;
		else			 pid = -1000 * (ichg+2) + ityp;

		int pdgID = -1;

		if (pid == 2106 or pid == -2106) {
//			double test = gRandom->Uniform(0.,1.);
			double test = double(rand() % 100)/100.;
			if (test >= 0.5) { pdgID = 10/*310*/; }    // K0S
			else { pdgID = 16/*130*/; }   // K0L
		} else
		// Convert Unique PID into PDG particle code 
		if (fParticleTable.find(pid) == fParticleTable.end()) {
			std::cerr << "Trig number: " << uevnum << ", particle number: " << itrack << std::endl;
			std::cerr << "-W CbmUrqmdGenerator: PID " << ityp << " charge " << ichg
			       << " not found in table (" << pid << ")" << std::endl;
			continue;
		} else {
			pdgID = fParticleTable[pid];
		}

//		++(*evt_);	TODO

		// Lorentztransformation to lab
		double mass = (double)(m);
		double px	 = (double)(ppx);
		double py	 = (double)(ppy);
		double pz	 = (double)(ppz);
		double e	= sqrt( mass*mass + px*px + py*py + pz*pz );
		pz = gamma * ( pz + beta * e );
		double ee = sqrt( mass*mass + px*px + py*py + pz*pz );

		int _itrack = itrack;//*evt_-1;
		uevt->ecm[_itrack] = e;
		uevt->elab[_itrack] = ee;
		uevt->px[_itrack] = px;
		uevt->py[_itrack] = py;
		uevt->pz[_itrack] = pz;
		uevt->x[_itrack] = x * 19e-13;
		uevt->y[_itrack] = y * 19e-13;
		uevt->z[_itrack] = z * 19e-13;
		uevt->pid[_itrack] = pdgID;
		uevt->pabs[_itrack] = sqrt(px*px + py*py + pz*pz);
		float add = 0.;
		if (y < 0.) add = M_PI;
		uevt->the[_itrack] = acos(pz/sqrt(pz*pz+py*py));
		uevt->phi[_itrack] = acos(px/sqrt(px*px + py*py)) + add;
		uevt->col[_itrack] = ncol;
/*		printf("C: %e %e %e %e %e %e\n",
			       	uevt->x[_itrack], uevt->y[_itrack], uevt->z[_itrack],
				uevt->px[_itrack], uevt->py[_itrack], uevt->pz[_itrack]);
*/
		// Rotate momenta by event plane angle
/*		if ( fEventPlaneSet ) {
			double pt = TMath::Sqrt(px*px + py*py);
			double azim = TMath::ATan2(py,px);
			azim += phi;
			px = pt * TMath::Cos(azim);
			py = pt * TMath::Sin(azim);
		}
*/

/*		TVector3 aa(px,py,pz);
		TLorentzVector pp;
		pp.SetPx( px );
		pp.SetPy( py );
		pp.SetPz( pz);
		pp.SetE( ee );
*/
	}
//	PR(__LINE__);
	return uevt;
}
