#ifndef URQMDEVENT_H
#define URQMDEVENT_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <map>
#include <iostream>

extern struct Files {
    char ifn[256];
	char ofn[256];
} inpargs;


#define PR(x) std::cout << "++DEBUG: " << #x << " = |" << x << "| in " << __FILE__ << " at line " << __LINE__ << "\n";

struct UrqmdEvent {
	const size_t parts;
	UrqmdEvent(size_t p) : parts(p), evtnum(0), Ebeam(0), bpar(0), iweight(0)
	{
		ncols = 0;

		x = new float[p];
		y = new float[p];
		z = new float[p];
		px = new float[p];
		py = new float[p];
		pz = new float[p];
		the = new float[p];
		phi = new float[p];
		pabs = new float[p];
		elab = new float[p];
		pid = new int[p];
		ecm = new float[p];
		col = new int[p];
	}

	virtual ~UrqmdEvent()
	{
		delete [] x;
		delete [] y;
		delete [] z;
		delete [] px;
		delete [] py;
		delete [] pz;
		delete [] the;
		delete [] phi;
		delete [] pabs;
		delete [] elab;
		delete [] pid;
		delete [] ecm;
		delete [] col;
	}

	float * x;
	float * y;
	float * z;
	float * px;
	float * py;
	float * pz;
	float * the;
	float * phi;
	float * pabs;
	float * elab;
	int * pid;
	float * ecm;
	int * col;

	int evtnum;
	float Ebeam;
	float bpar;
	float iweight;
	int ncols;
};

class UrqmdEventReader {
public:
	UrqmdEventReader();
	virtual ~UrqmdEventReader();

//	void ReadConversionTable();

	void openFile(const std::string & file);
	void closeFile();

	UrqmdEvent * getEvent();

private:
	long int counter;
	long int uevnum;

	FILE * fInputFile;
	std::map<int,int> fParticleTable;      //!  Map from UrQMD PID to PDGPID
	std::map<int,int> fParticleTableASCII;
};

#endif /* URQMDEVENT_H */
