#include <UrqmdEvent.h>

#include <iostream>
#include <cstdio>
#include <string>
#include <cstdio>
#include <getopt.h>

using namespace std;

int flag_verbose = 0;
int flag_filter = 0;

void convert(char * file)
{
	string inpfile, outfile;

	inpfile = file;
	outfile = inpfile + ".sfilter.evt";

	UrqmdEventReader uer;
	uer.openFile(inpfile);

	FILE * f = NULL, *ff = NULL;
	f = fopen(inpfile.c_str(), "r");
	if (!f) {
		cerr << "Unable to open output file!" << endl;
		exit(EXIT_FAILURE);
	}
	
	ff = fopen(outfile.c_str(), "w");
	if (!ff) {
		cerr << "Unable to open output file!" << endl;
		exit(EXIT_FAILURE);
	}
	
	cout << "Converting: " << inpfile << " -> " << outfile << endl;
	cout << "Filtering: " << flag_filter << endl;

	int levtnum = 0;
	int fevtnum = 0;
	int lastevtnum = 0;

	size_t parts;
	int evtnum, weight;
	float ebeam, bpar;

	int tmp1, tmp2;
	float tmp3;

	UrqmdEvent * evt = NULL;
	while ( !feof(f) ) {
//		PR(evt->evtnum);
//		lastevtnum = evt->evtnum;

		fscanf(f, "%d %d %f %f %d\n", &evtnum, &parts, &ebeam, &bpar, &weight);
		evt = new UrqmdEvent(parts);
		++lastevtnum;

//		printf("%d %f %f %d\n", parts, ebeam, bpar, weight);

		bool has_lambda = false;
		for (size_t s = 0; s < parts; ++s) {
			fscanf(f, " %e\t%e\t%e\t%e\t%d\t%d\t%d\t%e\n",
				&evt->elab[s], &evt->px[s], &evt->py[s], &evt->pz[s],
				&evt->pid[s], &tmp1, &tmp2, &tmp3);

			if (evt->pid[s] == flag_filter) {
				has_lambda = true;
			}
		}

		if (has_lambda) {
			++levtnum;
//			cout << evt->parts << "t" <<  endl;
			fprintf(ff, "%d\t%d\t%f\t%f\t%d\n",
					levtnum, parts, ebeam, bpar, weight);
			if (flag_verbose)
				printf("%d\t%d\t%f\t%f\t%d\n",
					levtnum, parts, ebeam, bpar, weight);

			for (size_t s = 0; s < parts; ++s) {
//				cout << "   " << evt->pid[s] << endl;
				fprintf(ff, " %e\t%e\t%e\t%e\t%d\t%d\t%d\t%e\n",
					evt->elab[s], evt->px[s], evt->py[s], evt->pz[s],
					evt->pid[s], tmp1, tmp2, tmp3);
				if (flag_verbose)
					printf(" %e\t%e\t%e\t%e\t%d\t%d\t%d\t%e\n",
						evt->elab[s], evt->px[s], evt->py[s], evt->pz[s],
						evt->pid[s], tmp1, tmp2, tmp3);
			}
		}
		delete evt;
		evt = NULL;
	}

	if (f) fclose(f);
	if (ff) fclose(ff);

	printf("Found lambda in %d/%d (%0.2f %%)\n", levtnum, lastevtnum, 100. * levtnum/lastevtnum);
}

int main(int argc, char ** argv)
{
	struct option lopt[] = {
		{ "verbose",	no_argument,		&flag_verbose,	1 },
		{ "filter",	required_argument,	0,		'f' },
		{ 0, 0, 0, 0 }
	};

	int c = 0;
	while (1) {
		int option_index = 0;
		c = getopt_long(argc, argv, "f:", lopt, &option_index);

		if (c == -1)
			break;

		switch (c) {
			case 'f':
				flag_filter = atoi(optarg);
				break;
			case 0:
				break;
			case '?':
				abort();
				break;
			default:
				break;
		}
	}

	if (!flag_filter) {
		cerr << "No filter selected!" << endl;
		exit(EXIT_FAILURE);
	}

	if (argc == optind) {
		cerr << "No input file!" << endl;
		exit(EXIT_FAILURE);
	}

	string inpfile;

	while (optind < argc) {
		convert(argv[optind]);
		++optind;
	}
}
