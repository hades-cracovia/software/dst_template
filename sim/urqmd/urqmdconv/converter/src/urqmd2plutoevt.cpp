#include <UrqmdEvent.h>
#include <iostream>
#include <cstdio>
#include <string>
#include <cstdio>
#include <vector>
#include <getopt.h>

using namespace std;

int flag_verbose = 0;
typedef std::pair<int, int> pid_filter;

std::vector<pid_filter> flag_filter;

int flag_full = 0;
int flag_strip = 1;
int flag_test = 0;
int flag_and = 0;
int flag_excl = 0;

const int collimit = 0;

bool checkevent(UrqmdEvent * e, int strip)
{
	bool is_ok = false;
	for (size_t s = 0; s < e->parts; ++s)
	{
		if (!strip or e->col[s]>collimit)
		{
			is_ok = true;
			break;
		}
	}

	return is_ok;
}

void fillheader(FILE * dst, UrqmdEvent * e, int evtcnt, int parts)
{
	if (!flag_test)
		fprintf(dst, "%d\t%d\t%f\t%f\t%d\n",
			evtcnt, parts, e->Ebeam, e->bpar, /*evt->iweight*/2);
	if (flag_verbose)
		printf("%d\t%d\t%f\t%f\t%d\n",
			evtcnt, parts, e->Ebeam, e->bpar, /*evt->iweight*/2);
}

void fillevent(FILE * dst, UrqmdEvent * e, int strip)
{
	for (size_t s = 0; s < e->parts; ++s)
	{
		if (!strip or e->col[s] > collimit)
		{
			if (!flag_test)
				fprintf(dst, "%e\t%e\t%e\t%e\t%d\t%d\t%d\t%e\n",
					e->elab[s], e->px[s], e->py[s], e->pz[s],
					e->pid[s], 0, 0, 1./3.);
			if (flag_verbose)
				printf("[%d] %e\t%e\t%e\t%e\t%d\t%d\t%d\t%e\n", s,
					e->elab[s], e->px[s], e->py[s], e->pz[s],
					e->pid[s], 0, 0, 1./3.);
		}
	}
}

void convert(char * file)
{
	string inpfile, outfile, outfilef;

	inpfile = file;
	outfile = inpfile + ".filter.evt";
	outfilef = inpfile + ".full.evt";

	UrqmdEventReader uer;
	uer.openFile(inpfile);

	FILE * f = NULL, *ff = NULL;

	if (!flag_test and flag_filter.size())
   	{
		f = fopen(outfile.c_str(), "w");
		if (!f) {
			cerr << "Unable to open output file!" << endl;
			exit(EXIT_FAILURE);
		}
	}

	if (!flag_test and flag_full)
	{
		ff = fopen(outfilef.c_str(), "w");
		if (!ff)
		{
			cerr << "Unable to open output file!" << endl;
			exit(EXIT_FAILURE);
		}
	}

	cout << "Converting: " << inpfile << " -> " << outfile << endl;
	cout << "Filtering:";
	for (size_t s = 0; s < flag_filter.size(); ++s)
		cout << " " << flag_filter[s].first;
	cout << endl;

	cout << "Stripping: " << flag_strip << endl;
	cout << "Full output: " << flag_full << endl;

	int levtnum = 0;
	int fevtnum = 0;
	int lastevtnum = 0;
	int evexcl = 0;

	const size_t filters_number = flag_filter.size();
	bool * filter_found = new bool[filters_number];

	UrqmdEvent * evt = NULL;
	while ( (evt = uer.getEvent()) )
   	{
		if (flag_excl and (evt->parts != filters_number))
			continue;

		for (size_t s2 = 0; s2 < filters_number; ++s2)
			filter_found[s2] = false;

		lastevtnum = evt->evtnum;

		int nonintcnt = 0;
		bool has_requested = false;

		for (size_t s = 0; s < evt->parts; ++s)
	   	{
			for (size_t s2 = 0; s2 < filters_number; ++s2)
				if ( (evt->pid[s] == flag_filter[s2].first))
				{
					filter_found[s2] = true;
					has_requested = true;
					++flag_filter[s2].second;
//					break;
				}

			// if stripping requested
			if (flag_strip)
		   	{
				// increase counter when has collision
				if (evt->col[s]>collimit)
					++nonintcnt;
			} else {
				++nonintcnt;
			}
		}

		bool is_ok = checkevent(evt, flag_strip);

		if (flag_and)
		{
			has_requested = true;
			for (size_t s2 = 0; s2 < filters_number; ++s2)
				has_requested = has_requested && filter_found[s2];
		}

		if (flag_excl and has_requested)
			++evexcl;

		if (flag_strip and is_ok)
		{
			// full output 
			if (flag_full)
			{
				fillheader(ff, evt, ++fevtnum, nonintcnt);
				fillevent(ff, evt, flag_strip);
			}

			if (has_requested)
			{
				fillheader(f, evt, ++levtnum, nonintcnt);
				fillevent(f, evt, flag_strip);
			}
		}
		delete evt;
		evt = NULL;
	}

	if (f) fclose(f);
	if (ff) fclose(ff);

	printf("Analysed %d events\n", lastevtnum);
//	if (flag_filter.size())
//		printf("Found pid=%d in %d/%d (%0.2f %%)\n", flag_filter, levtnum, lastevtnum, 100. * levtnum/lastevtnum);
	for (size_t s = 0; s < flag_filter.size(); ++s)
		printf("Found pid=%d in %d/%d (%0.2f %%)\n", flag_filter[s].first, flag_filter[s].second, lastevtnum, 100. * flag_filter[s].second/lastevtnum);
	cout << "Exclusive events [" << flag_excl << "]: " << evexcl << endl;
}

void usage(char ** argv)
{
	std::cout << "Usage: " << "\t"
		<< argv[0] << " [options] ft14_file(s)" << std::endl
		<< "\tOptions:" << std::endl
		<< "\t--verbose\t-- speak a bit more" << std::endl
		<< "\t--filter pid\t-- export only events where particle with PID=pid exists" << std::endl
		<< "\t--full\t\t-- export full output, default when no --filter selected, otherwise must be set explicite" << std::endl
		<< "\t--nostrip\t-- remove noninteracting particles (ie. in HIC) from the output" << std::endl
		<< "\t--test\t\t-- do not create output file, only simulate" << std::endl
		<< " Note: --full and --filter can be used at the same time" << std::endl;
}

int main(int argc, char ** argv)
{
	struct option lopt[] = {
		{ "verbose",	no_argument,		&flag_verbose,	1 },
		{ "full",	no_argument,		&flag_full,	1 },
		{ "filter",	required_argument,	0,		'f' },
		{ "and", no_argument, &flag_and, 1 },
		{ "exclusive", no_argument, &flag_excl, 1 },
		{ "nostrip",	no_argument,		&flag_strip,	0 },
		{ "test",	no_argument,		&flag_test,	1 },
		{ "help",	no_argument,		0,		'h' },
		{ "usage",	no_argument,		0,		'u' },
		{ 0, 0, 0, 0 }
	};

	int c = 0;
	while (1) {
		int option_index = 0;
		c = getopt_long(argc, argv, "f:h", lopt, &option_index);

		if (c == -1)
			break;

		switch (c) {
			case 'f':
				flag_filter.push_back( pid_filter(atoi(optarg), 0) );
				break;
			case 0:
				break;
			case 'u':
			case 'h':
				usage(argv);
				std::exit(EXIT_SUCCESS);
				break;
			case '?':
				abort();
				break;
			default:
				break;
		}
	}

	if (!flag_filter.size() and !flag_full)
		flag_full = 1;

	if (argc == optind) {
		cerr << "No input file!" << endl;
		exit(EXIT_FAILURE);
	}

	string inpfile, outfile;

	while (optind < argc) {
		convert(argv[optind]);
		++optind;
	}
}
