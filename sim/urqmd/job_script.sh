#!/bin/bash

exp="dst_template"

date

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY: ${SLURM_ARRAY_TASK_ID} in ${pattern}"
    file=$(sed -n $((${SLURM_ARRAY_TASK_ID}+1))p ${pattern})
else
    echo "NO ARRAY"
    file=$pattern
fi

. /lustre/nyx/hades/user/${USER}/hades/${exp}/profile.sh

odir=$(readlink -e $odir)

card=AgAg165
seed=$file
echo card=$card
echo file=$file
echo events=$events
echo odir=$odir
root -b -q

cd /lustre/nyx/hades/user/${USER}/hades/${exp}/sim/urqmd

wd=$(pwd)

card_file=/tmp/urqmd_card_${USER}__${card}__${file}.dat

date

ofile=${card}_seed_${seed}

truncate -c -s0 "${odir}/${ofile}.*"

# update placeholders and generate temporary file
sed \
    -e "s|@seed@|${seed}|" \
    ${card}.dat > ${card_file}

tmpdirname=$(mktemp -d)
cd ${tmpdirname}
pwd

# run urqmd
echo ${wd}/urqmd $card_file $ofile
time ${wd}/urqmd $card_file $ofile

rename ftn1 ftn14 $ofile.ftn1

# run urqmd -> pluto converter
flags="--filter 18"

export LD_LIBRARY_PATH=${wd}/urqmdconv/install/lib/:${LD_LIBRARY_PATH}
export PATH=${wd}/urqmdconv/install/bin/:${PATH}

time urqmd2plutoevt $ofile.ftn14 $flags
#rename "ftn14.full" "" $ofile.ftn*.evt
rename "ftn14.filter" "" $ofile.ftn*.evt

mv ${ofile}* ${odir}/ -v

echo "*********************************************"
cat ${card_file}
echo "*********************************************"

cd -
rm -rfv ${tmpdirname}
rm -fv ${card_file}
